import Vue from 'vue/dist/vue.esm.js';
// https://www.digitalocean.com/community/tutorials/vuejs-global-event-bus
// console.log('%cVue EventBus installed!!', 'font-family:fantasy;font-size:24px;color:#4fc08d;font-weight:700;');
export const EventBus = new Vue(),
    EventName = "vue-event-bus", // generic bus events
    GTMEvent = "gtm-event"; // GTM-specific event bus