// https://www.digitalocean.com/community/tutorials/vuejs-global-event-bus
import { EventBus, GTMEvent } from './event-bus';
import ENV_VARS from "environment-variables"; // from the project's `.env`


/* 

  DataLayer Object Schema = {
            "event": [String; App-wide token ],
            "event_category": [String; App module signature token ],
            "event_action": [String; A terse description of the triggering action],
            "event_label": [Any; details/data about the action]
        }
*/

const APP_REFERENCE = ENV_VARS.env_id; // dynamically reference the project's ID from the `.env`

window.dataLayer = window.dataLayer || [];

EventBus.$on( GTMEvent, payload => {
    // console.log( `Looks like "${payload.reference}" has something to say`, {
    //     "event": APP_REFERENCE,
    //     "event_category": payload.reference,
    //     "event_action": payload.action,
    //     "event_label": payload.data
    // }, window.dataLayer );

    window.dataLayer = window.dataLayer || [];

    /*
    `payload` Object Schema
    {
        reference: [String, token that identifies the source-component]
        action: [String, action name],
        data: [String/Object/Array, data associated with action]
    }
    */
    window.dataLayer.push( {
        "event": APP_REFERENCE,
        "event_category": payload.reference,
        "event_action": payload.action,
        "event_label": payload.data
    } );

  });


