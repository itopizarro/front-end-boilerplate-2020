export default {
  props: {
    hierarchy: {
      type: [Number, String],
      default() {
        return 2;
      },
    },
  },
  methods:{
    $validateObjectArray(array, property) {
      let objectsWithProperty;

      // No Array? Jail.
      if (!array) {
        return false;
      }

      // No property? Jail.
      if (!property) {
        return false;
      }

      // Ok. Do any of the items in the Array even have this property?
      objectsWithProperty = array.filter((current) => {
        return Object.prototype.hasOwnProperty.call(current, property);
      });

      // No items with that property? Jail.
      if (!objectsWithProperty.length) {
        return false;
      }

      // otherwise…
      return objectsWithProperty;
    },

    $sortArrayByChildProperty(array, property) {
      if (!this.$validateObjectArray(array, property)) {
        return [];
      }

      return array.sort((a, b) => {
        let valueA = a[property],
          valueB = b[property];
        if (valueA < valueB) {
          return -1;
        }
        if (valueA > valueB) {
          return 1;
        }
        return 0;
      });
    },

    $getObjectFromArrayByPropertyValue(array, property, value) {
      let validatedObject = this.$validateObjectArray(array, property);

      if (!validatedObject) {
        return [];
      }

      // No value? Well. Less jail. But keep 'yer nose clean, unnerstan'?
      if (!value) {
        return validatedObject;
      }

      // Ok. Now down to business.
      return array.filter((object) => {
        return object[property] == value;
      });
    },

    $hierarchyTagCalculator(number) {
      switch (parseInt(number, 10)) {
        // https://stackoverflow.com/a/6114242/5796134
        case 5:
        case 4:
        case 3:
        case 2:
          return `h${number}`;
        default:
          return "p";
      }
    },
  },
  computed: {
    $isMobile() {
      //   console.log({ isMobile: this.$mq });
      return this.$mq == "xs" || this.$mq == "sm";
    },
  },
  mounted() {
    console.log(`MOUNTED: ${this.$options.name}`);
  },
}