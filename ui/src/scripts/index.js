// index.js
// Kicks off any/all sitewide JS and modular JS components
"use strict";

/* 
    NOTE: Webpack is configured to automatically try to resolve imported files in the following directories:
    * `node_modules/` 
    * `ui/src/scripts/modules/` 
    * `ui/src/scripts/vendor/` 
    * `ui/src/scripts/models/` 
    * `ui/src/scripts/components/`
*/

// Environment Variables
import ENV_VARS from "environment-variables"; // from the project's `.env`

// (S)CSS
import "./../scss/main.scss";

// Vue Utilities
// Vue-ish Event Bus (https://www.digitalocean.com/community/tutorials/vuejs-global-event-bus)
import { EventBus, EventName, GTMEvent } from "./event-bus.js";

// A utility for pushing GTM events via the Vue-is Event Bus
import { PushEvent } from "./gtm-dataLayer";

// A library of commonly needed utilities (so they're accessible to all Vue components)
import Mixins from "./modules/mixins.js";

// A library for handling media queries as reactive data in Vue
import VueMq from "vue-mq";

// Global Utilities/Libraries:
// NOTE: Truly ubiquitous Libraries *should* be added to Webpack"s awareness via it"s `resolve.alias`
import "foo.js"; // REMEMBER: Webpack knows to look in `vendor/` to resolve this filepath

// Custom Modules
import Bar from "bar.js"; // REMEMBER: Webpack knows to look in `modules/` to resolve this filepath

// JSON data
import SomeData from "./constants/some-data.json";

// A Vue component
import Baz from "./components/baz.vue";

// Register Vue Extensions/Utilities
Vue.use(VueMq, {
  breakpoints: {
    min: 0,
    xs: 480,
    sm: 768,
    md: 992,
    lg: 1024,
    xl: 1400,
    max: "Infinity",
  },
  defaultBreakpoint: "sm", // customize this for SSR
});
Vue.mixin(Mixins);

// Create a Window object
window.app = window.app || {};

(function() {
  // do stuff…

  console.log("Yay! You're running the 2020 Boilerplate!", {
    Vue,
    ENV_VARS,
    SomeData,
  });

  // use a method from `foo.js`
  window.Foo();

  // Importing an ES6 Class module
  let bar = new Bar();
  bar.init();

  // Init a sitewide Vue model
  new Vue({
    name: "Main",
    el: "#mount-point",
    components: { Baz },
    mounted() {
        console.log( `Vue model ${this.$options.name} has mounted`, { "view-model": this });
      // Emit an event to tell GTM that the App loaded
      EventBus.$emit(GTMEvent, {
        reference: this.$options.name,
        action: `${this.$options.name} mounted`,
        data: {
          event: "mounted",
          timestamp: Date.now(),
        },
      });
    },
  });
})();
