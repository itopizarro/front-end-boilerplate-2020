const ENV = __ENV__;
module.exports = {
    "env_id": ENV.env_id,
    "gtm_id": ENV.gtm_id,
    "project_ref": ENV.project_ref
};