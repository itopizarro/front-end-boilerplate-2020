// modules/foo.js
// This is an example of a library of plugin that does not return a class or module instance
window.Foo = function () {
    console.log( 'THIS IS THE EXAMPLE VENDOR LIBRARY' );
}