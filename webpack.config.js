const 
  chalk = require("chalk"),
  path = require("path"),
  webpack = require("webpack"),
  project_config = require("./project.config.json"),
  CopyPlugin = require("copy-webpack-plugin"),
  MiniCssExtractPlugin = require("mini-css-extract-plugin"),
  VueLoaderPlugin = require("vue-loader/lib/plugin");

let webpack_config;

// Getting .env variables
require("dotenv").config();

function ConfigPath() {
  let root = project_config[this.type];
  if (typeof root[this.token] != "undefined") {
    return root[this.token].path === null
      ? project_config.theme.path + root.path
      : project_config.theme.path + root.path + root[this.token].path;
  }
  return project_config.theme.path + root.path;
}

function get_asset_path(token) {
  this.type = "asset";
  this.token = token;
  return ConfigPath.call(this);
}
function get_asset_directory(token) {
  let root = project_config["asset"];
  if (typeof root[token] != "undefined") {
    return typeof root[token].path != "undefined"
      ? root[token].path
      : root.path;
  }
  return root.path;
}
function get_source_path(token) {
  this.type = "source";
  this.token = token;
  return ConfigPath.call(this);
}
function get_entry() {
  let entry_path = project_config.source.scripts.build.path,
    root = get_source_path("scripts");
  if (!entry_path.length) {
    return root + project_config.source.scripts.build.entry;
  }
  return root + entry_path + project_config.source.scripts.build.entry;
}


webpack_config = {
  entry: get_entry(),
  output: {
    filename: "[name].build.js",
    path: path.resolve(__dirname, get_asset_path()),
  },
  resolve: {
    alias: {
      "/node_modules": path.resolve(__dirname, "./node_modules"),
    },
    modules: [
      path.resolve(
        __dirname,
        get_source_path( "scripts" ) + "components"
      ),
      path.resolve(
        __dirname,
        get_source_path( "scripts" ) + "models"
      ),
      path.resolve(
        __dirname,
        get_source_path( "scripts" ) + "constants"
      ),
      path.resolve(
        __dirname,
        get_source_path( "scripts" ) + "modules"
      ),
      path.resolve(
        __dirname,
        get_source_path( "scripts" ) + "vendor"
      ),
      path.resolve(
        __dirname, 
        "node_modules"
      )
    ]
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
            plugins: ["@babel/plugin-transform-runtime"],
          },
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Extract CSS to its own file
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: path.resolve(__dirname, get_asset_path()),
            },
          },

          // Translates CSS into CommonJS
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true,
              config: {
                path: "./postcss.config.js",
              },
            },
          },
          // Compiles Sass to CSS
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: get_asset_directory("images"),
              publicPath: "./" + get_asset_directory("images"),
              name: "[name].[ext]",
            },
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: get_asset_directory("fonts"),
              publicPath: "./" + get_asset_directory("fonts"),
              name: "[name].[ext]",
            },
          },
        ],
      },
      {
        test: /\.vue$/,
        use: [
          {
            loader: "vue-loader",
          },
        ],
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: get_source_path("images"),
          to: get_asset_directory("images"),
        },
      ],
    }),
    new webpack.DefinePlugin({
      __ENV__: {
        env_id: JSON.stringify(process.env.ID),
        gtm_id: JSON.stringify(process.env.GTM),
        project_ref: JSON.stringify(process.env.REFERENCE)
      },
    }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].build.css",
      chunkFilename: "[name].build.chunk-[id].css",
    }),
    new webpack.ProvidePlugin({
      Axios: "axios",
      Vue: ["vue/dist/vue.esm.js", "default"],
    }),
    new VueLoaderPlugin(),
  ],
};

module.exports = (environment, argument_variables) => {
  console.log( chalk.black.bgGreenBright( `WebPacking in ${argument_variables.mode} mode` ) );
  // settings for development mode
  if (argument_variables.mode === "development") {

    webpack_config.devtool = "source-map";
    webpack_config.watch = true;
  }
  
  // settings for production mode
  if (argument_variables.mode === "production") {
    // stuff
  }

  return webpack_config;
};

