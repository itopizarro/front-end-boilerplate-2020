# Front End Boilerplate: 2020-ish
A WebPack/Babel build with Vue (2)

## Getting started
1. In a terminal/command-line, navigate to your project's directory
2. Run `nvm install`; this installs the appropriate version of NodeJS, if needed, and sets it as the current version. You can check that with `nvm current`.
    1. You have [NVM](https://github.com/nvm-sh/nvm#installing-and-updating), right?
3. Run `npm install` to load up all the Node Packages indicated in the repo's `package.json`
4. If the Front End UI source files and/or output (AKA "dist") directory are somewhere _other_ than `./ui/src` and `./ui/dist`, you'll want to crack open the `project.config.json`
    1. If you're using a WP theme, put the path to the theme's directory in `theme.path`. The build process will prefix that to the `source` and `asset` paths.
    2. If either your `source` or `asset` (AKA "dist" or "output") directory paths do not match the default &mdash; `ui/[src|dist]` &mdash; pattern, update them in `source.path` and `asset.path`
    3. Remember that these paths are relative to the project's root directory!
5. Continue updating `[property].path` values as necessary.
6. Edit the project's `.env` to reflect appropriate values for your build
    1. **GTM** (currently not used; future feature) is a placeholder for the site's Google Tag Manager ID.
    2. **ID** is a descriptor of the server environment &mdash; this is designed to be edited (then `git update-index --skip-worktree`) in the context of the server(s) it resides on. *E.g. on a QA1 box, it might have the value `QA1`
    3. **REFERENCE** this is used to identify the site to GTM in the `ui/src/scripts/gtm-dataLayer.js` utility. You're going to want to change it to the client/project name.
7. **Now you're ready to test the build!** Run `npm run build` to kick-off a one-time, "production" build. If there are no errors during the build, check your `dist` folder to see if you now have some compiled files.
8. While you're developing, dedicate a terminal window to the `develop` process by running `npm run develop`. This will set file watchers on all of your JS and SCSS files; automatically triggering re-builds whenever you save a file.
